
import java.util.Vector;

class Database {

	static final String CHARACTER_SET = "utf8";
	static final String COLLATE = "utf8_unicode_ci";
	static final String ON_DELETE = "CASCADE";
	static final String ON_UPDATE = "CASCADE";
	//
	String name = null;
	Vector<Table> tables = new Vector<Table>();

	Database(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder sbr = new StringBuilder();
		sbr.append(String.format("-- Table-Count: %s\n", tables.size()));
		sbr.append(String.format("CREATE DATABASE IF NOT EXISTS `%s` CHARACTER SET '%s' COLLATE '%s';\n",
				name, CHARACTER_SET, COLLATE));
		for (int i = 0; i < tables.size(); i++) {
			Table table = tables.elementAt(i);
			sbr.append(table.toString());
		}
		{
			int count = 0;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < tables.size(); i++) {
				Table table = tables.elementAt(i);
				if (table.note != null) {
					StringBuilder s = new StringBuilder();
					s.append(String.format("INSERT INTO `%s`.`%s` (", name, table.name));
					for (int j = 0; j < table.attributes.size(); j++) {
						Attribute attribute = table.attributes.elementAt(j);
						if (j > 0) {
							s.append(", ");
						}
						s.append(String.format("`%s`", attribute.name));
					}
					String notes[] = table.note.data.replaceAll("\r", "").split("\n");
					for (String note : notes) {
						count++;
						sb.append(s.toString());
						sb.append(String.format(") VALUES %s;\n", note));
					}
				}
			}
			if (count > 0) {
				sbr.append("\n");
				sbr.append(String.format("-- Insert-Count: %s\n", count));
				sbr.append(sb.toString());
			}
		}
		{
			StringBuilder sb = new StringBuilder();
			int count = 0;
			for (int i = 0; i < tables.size(); i++) {
				Table table = tables.elementAt(i);
				for (int j = 0; j < table.attributes.size(); j++) {
					Attribute attribute = table.attributes.elementAt(j);
					if (attribute.foreignKey != null) {
						count++;
						if (attribute.isPrimaryKey || attribute.isUnique) {
							sb.append(String.format("-- Notice: statement below may interrupt when import\n"));
						}
						sb.append(String.format("ALTER TABLE `%1$s`.`%2$s` ADD CONSTRAINT `%2$s_%3$s` FOREIGN KEY (`%4$s`) REFERENCES `%1$s`.`%5$s` (`%6$s`) ON DELETE %7$s ON UPDATE %8$s;\n",
								name, table.name, String.format("%09d", Dia2MySQL.generateRandomNumber()), attribute.name,
								attribute.foreignKey.table.name, attribute.foreignKey.name, ON_DELETE, ON_UPDATE));
					}
				}
			}
			if (count > 0) {
				sbr.append("\n");
				sbr.append(String.format("-- Foreign-Count: %s\n", count));
				sbr.append(sb.toString());
			}
		}
		return sbr.toString();
	}
}
