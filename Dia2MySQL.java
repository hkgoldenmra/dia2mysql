
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Dia2MySQL {

	static long generateRandomNumber() {
		return new Double(new Random(System.currentTimeMillis()).nextDouble() * Math.random() * 1000000000l).longValue();
	}
	//
	static final String AUTHOR = "Mr.A";
	static final String REGEX_PATTERN = "[\\*\\+\\.\\(\\)\\[\\]\\{\\}\\<\\>\\? -]";
	//
	private Database database;

	public Dia2MySQL(File file, String dbName) throws IOException, ParserConfigurationException, SAXException, IllegalArgumentException {
		if (dbName == null) {
			dbName = file.getName();
		}
		dbName = dbName.replaceAll(REGEX_PATTERN, "_");
		database = new Database(dbName);
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		builder.setErrorHandler(null);
		Element element = null;
		try {
			element = builder.parse(file).getDocumentElement();
		} catch (Exception ex1) {
			try {
				element = builder.parse(new GZIPInputStream(new FileInputStream(file))).getDocumentElement();
			} catch (Exception ex2) {
				throw new IllegalArgumentException(ex2);
			}
		}
		getTables(element);
		getNotes(element);
		getForeignKeys(element);
	}

	public Dia2MySQL(File file) throws IOException, ParserConfigurationException, SAXException, IllegalArgumentException {
		this(file, null);
	}

	public static void main(String args[]) {
		if (args.length > 0) {
			File file = new File(args[0]);
			String dbName = file.getName();
			int index = dbName.lastIndexOf('.');
			if (index > -1) {
				dbName = dbName.substring(0, index);
			}
			dbName.replaceAll(REGEX_PATTERN, "_");
			String filename = dbName;
			if (args.length > 1) {
				if (args[1].endsWith(".sql")) {
					filename = args[1];
				} else {
					filename = args[1] + ".sql";
				}
			} else {
				filename += ".sql";
			}
			File export = new File(filename);
			if (export.isFile()) {
				System.out.printf("overwrite file \"%s\" (y/N) ? ", export.getAbsolutePath());
				String line = new Scanner(System.in).nextLine();
				if (!line.matches("^[Yy]$")) {
					System.out.println("terminated");
					System.exit(0);
				}
			} else if (export.isDirectory()) {
				System.err.printf("\"%s\" is a directory\n", export.getAbsolutePath());
				System.exit(0);
			}
			try {
				System.out.printf("parsing file \"%s\" to \"%s\" ...\n", file.getAbsolutePath(), export.getAbsolutePath());
				Dia2MySQL dia2mysql = new Dia2MySQL(file, dbName);
				StringBuilder sbr = new StringBuilder();
				sbr.append(String.format("-- Author: %s\n"
						+ "-- Build-On: %s\n"
						+ "-- Java-Version: %s\n"
						+ "-- OS: %s\n",
						AUTHOR, new SimpleDateFormat("YYYY-MM-dd HH:mm:ss.SSS").format(new Date()),
						System.getProperty("java.version"), System.getProperty("os.name")));
				sbr.append("\n");
				sbr.append("/*\n");
				sbr.append(String.format("if you don't need to create database\n"
						+ "you can comment the create database statement\n"
						+ "and use this command: \":%%s/`%s`\\.//g\" in vim to erase the database prefix\n", dia2mysql.database.name));
				sbr.append("\n");
				sbr.append("all foreign keys and duplicated or unnamed tables or attributes\n"
						+ "will append a 9-digit number randomly with underscore prefix\n"
						+ "you should rename them if necessary\n");
				sbr.append("\n");
				sbr.append(String.format("import the generated sql file using command: \"mysql -u <db-user> -p < '%s'\"\n"
						+ "<db-user> is the name of your database user not actual type <db-user>\n", export.getAbsolutePath()));
				sbr.append("*/\n");
				sbr.append("\n");
				sbr.append("-- if you want to renew database settings, you may uncomment statement below\n");
				sbr.append(String.format("-- DROP DATABASE `%s`;\n", dia2mysql.database.name));
				sbr.append("\n");
				sbr.append(dia2mysql.database.toString());
				String sql = sbr.toString();
				int length = 1024;
				int start = 0;
				BufferedWriter out = new BufferedWriter(new FileWriter(export));
				while (start < sql.length()) {
					out.write(sql, start, start + length < sql.length() ? length : sql.length() - start);
					start += length;
				}
				out.flush();
				out.close();
				System.out.println("completed");
//			} catch (IllegalArgumentException ex) {
//				System.err.printf("\"%s\" is not a valid Dia XML or Dia GZip compressed XML file\n", file.getAbsolutePath());
//			} catch (FileNotFoundException ex) {
//				System.err.println(ex.toString());
//			} catch (IOException ex) {
//				System.err.println("I/O error");
//			} catch (ParserConfigurationException ex) {
//				System.err.println("XML parser error");
//			} catch (SAXException ex) {
//				System.err.printf("\"%s\" is not a XML file\n", file.getAbsolutePath());
			} catch (Exception ex) {
				System.err.println(ex.toString());
			}
		} else {
			System.out.printf("java %s <in.dia> [out.sql]\n", Dia2MySQL.class.getName());
			System.exit(0);
		}
	}

	private void getTables(Element element) {
		NodeList diaObjects = element.getElementsByTagName("dia:object");
		for (int i = 0; i < diaObjects.getLength(); i++) {
			Element diaObject = (Element) (diaObjects.item(i));
			if (diaObject.getAttribute("type").equals("Database - Table")) {
				Table table = new Table(database);
				table.code = diaObject.getAttribute("id");
				NodeList diaAttributes = diaObject.getChildNodes();
				for (int j = 0; j < diaAttributes.getLength(); j++) {
					Node diaAttribute = diaAttributes.item(j);
					if (diaAttribute.getNodeType() == Node.ELEMENT_NODE) {
						Element tableAttribute = (Element) diaAttribute;
						String name = tableAttribute.getAttribute("name");
						if (name.equals("name")) {
							String data = tableAttribute.getElementsByTagName("dia:string").item(0).getTextContent();
							data = data.substring(1, data.length() - 1).replaceAll(REGEX_PATTERN, "_");
							table.name = data;
							// this is not a perfect solution to solve duplicated tables
							boolean duplicated = false;
							do {
								for (Table dbTable : database.tables) {
									if (duplicated = dbTable.name.equals(table.name)) {
										break;
									}
								}
								if (duplicated || table.name.length() == 0) {
									table.name = String.format("%s_%09d", data, Dia2MySQL.generateRandomNumber());
								}
							} while (duplicated);
							System.out.printf("get table `%s`\n", table.name);
						} else if (name.equals("comment")) {
							String data = tableAttribute.getElementsByTagName("dia:string").item(0).getTextContent();
							table.comment = data.substring(1, data.length() - 1);
						} else if (name.equals("attributes")) {
							getAttributes(table, tableAttribute);
						}
					}
				}
				if (table.name != null) {
					database.tables.addElement(table);
				}
			}
		}
	}

	private void getAttributes(Table table, Element element) {
		NodeList diaComposites = element.getElementsByTagName("dia:composite");
		for (int i = 0; i < diaComposites.getLength(); i++) {
			Attribute attribute = new Attribute(table);
			NodeList diaAttributes = ((Element) (diaComposites.item(i))).getElementsByTagName("dia:attribute");
			for (int j = 0; j < diaAttributes.getLength(); j++) {
				Element diaAttribute = (Element) (diaAttributes.item(j));
				String name = diaAttribute.getAttribute("name").toLowerCase();
				if (name.equals("name")) {
					String data = diaAttribute.getElementsByTagName("dia:string").item(0).getTextContent();
					data = data.substring(1, data.length() - 1).replaceAll(REGEX_PATTERN, "_");
					attribute.name = data;
					// this is not a perfect solution to solve duplicated attrbutes
					boolean duplicated = false;
					do {
						for (Attribute dbAttribute : table.attributes) {
							if (duplicated = dbAttribute.name.equals(attribute.name)) {
								break;
							}
						}
						if (duplicated || attribute.name.length() == 0) {
							attribute.name = String.format("%s_%09d", data, Dia2MySQL.generateRandomNumber());
						}
					} while (duplicated);
					System.out.printf("get attribute `%s`\n", attribute.name);
				} else if (name.equals("type")) {
					String data = diaAttribute.getElementsByTagName("dia:string").item(0).getTextContent();
					attribute.type = data.substring(1, data.length() - 1);
				} else if (name.equals("comment")) {
					String data = diaAttribute.getElementsByTagName("dia:string").item(0).getTextContent();
					attribute.comment = data.substring(1, data.length() - 1);
				} else if (name.equals("primary_key")) {
					attribute.isPrimaryKey = ((Element) (diaAttribute.getElementsByTagName("dia:boolean").item(0))).getAttribute("val").toLowerCase().equals("true");
				} else if (name.equals("nullable")) {
					attribute.isNullable = ((Element) (diaAttribute.getElementsByTagName("dia:boolean").item(0))).getAttribute("val").toLowerCase().equals("true");
				} else if (name.equals("unique")) {
					attribute.isUnique = ((Element) (diaAttribute.getElementsByTagName("dia:boolean").item(0))).getAttribute("val").toLowerCase().equals("true");
				}
			}
			if (attribute.name != null) {
				table.attributes.addElement(attribute);
			}
		}
	}

	private void getForeignKeys(Element element) {
		NodeList diaObjects = element.getElementsByTagName("dia:object");
		for (int i = 0; i < diaObjects.getLength(); i++) {
			Element diaObject = (Element) (diaObjects.item(i));
			if (diaObject.getAttribute("type").equals("Database - Reference")) {
				NodeList connections = diaObject.getElementsByTagName("dia:connection");
				Element connectionFrom = (Element) (connections.item(1));
				Element connectionTo = (Element) (connections.item(0));
				Attribute attributeFrom = null;
				Attribute attributeTo = null;
				for (int j = 0; j < database.tables.size(); j++) {
					Table table = database.tables.elementAt(j);
					if (table.code.equals(connectionFrom.getAttribute("to"))) {
						int indexFrom = Integer.parseInt(connectionFrom.getAttribute("connection")) / 2 - 6;
						attributeFrom = table.attributes.elementAt(indexFrom);
					}
					if (table.code.equals(connectionTo.getAttribute("to"))) {
						int indexTo = Integer.parseInt(connectionTo.getAttribute("connection")) / 2 - 6;
						attributeTo = table.attributes.elementAt(indexTo);
					}
					if (attributeFrom != null && attributeTo != null) {
						attributeFrom.foreignKey = attributeTo;
						System.out.printf("get foreign key `%s.%s` > `%s.%s`\n", attributeFrom.table.name, attributeFrom.name, attributeTo.table.name, attributeTo.name);
						break;
					}
				}
			}
		}
	}

	private void getNotes(Element element) {
		NodeList diaObjects = element.getElementsByTagName("dia:object");
		for (int i = 0; i < diaObjects.getLength(); i++) {
			Element diaObject = (Element) (diaObjects.item(i));
			if (diaObject.getAttribute("type").equals("Standard - ZigZagLine")) {
				NodeList connections = diaObject.getElementsByTagName("dia:connection");
				String idFrom = ((Element) (connections.item(1))).getAttribute("to");
				String idTo = ((Element) (connections.item(0))).getAttribute("to");
				Note noteFrom = null;
				Table tableTo = null;
				for (int j = 0; j < database.tables.size(); j++) {
					Table table = database.tables.elementAt(j);
					if (table.code.equals(idFrom)) {
						tableTo = table;
					}
				}
				for (int j = 0; j < diaObjects.getLength(); j++) {
					Element diaObj = (Element) (diaObjects.item(j));
					if (diaObj.getAttribute("type").equals("UML - Note") && diaObj.getAttribute("id").equals(idTo)) {
						NodeList diaAttributes = diaObj.getElementsByTagName("dia:attribute");
						for (int k = 0; k < diaAttributes.getLength(); k++) {
							Element diaAttribute = (Element) (diaAttributes.item(k));
							String name = diaAttribute.getAttribute("name");
							if (name.equals("string")) {
								noteFrom = new Note();
								String data = diaAttribute.getElementsByTagName("dia:string").item(0).getTextContent();
								data = data.substring(1, data.length() - 1);
								noteFrom.data = data;
								break;
							}
						}
					}
				}
				if (noteFrom != null && tableTo != null) {
					noteFrom.table = tableTo;
					tableTo.note = noteFrom;
					int length = noteFrom.data.split("\n").length;
					System.out.printf("get entry `%s` > %s entr%s\n", tableTo.name, length, (length > 1 ? "ies" : "y"));
				}
			}
		}
	}
}
