
import java.util.Vector;

class Table {

	static final String ENGINE = "InnoDB";
	static final String DEFAULT_CHARSET = "utf8";
	static final String COLLATE = "utf8_unicode_ci";
	static final int AUTO_INCREMENT = 1;
	//
	// dia xml reference
	String code = null;
	Database database = null;
	String name = null;
	String comment = null;
	Vector<Attribute> attributes = new Vector<Attribute>();
	Note note = null;

	Table(Database database) {
		this.database = database;
	}

	public String toString() {
		StringBuilder sbr = new StringBuilder();
		sbr.append("\n");
		sbr.append(String.format("-- Attribute-Count: %s\n", attributes.size()));
		sbr.append(String.format("CREATE TABLE IF NOT EXISTS `%s`.`%s` (\n", database.name, name));
		boolean hasPrimaryKey = false;
		for (int i = 0; i < attributes.size(); i++) {
			Attribute attribute = attributes.elementAt(i);
			sbr.append(String.format("\t%s%s", i > 0 ? ", " : "", attribute.toString()));
			hasPrimaryKey |= attribute.isPrimaryKey;
		}
		if (hasPrimaryKey) {
			sbr.append("\t, PRIMARY KEY (");
			for (int i = 0; i < attributes.size(); i++) {
				Attribute attribute = attributes.elementAt(i);
				boolean filled = false;
				if (attribute.isPrimaryKey) {
					sbr.append(String.format("%s`%s`", filled ? ", " : "", attribute.name));
					filled = true;
				}
			}
			sbr.append(")\n");
		}
		for (int i = 0; i < attributes.size(); i++) {
			Attribute attribute = attributes.elementAt(i);
			if (attribute.isUnique && !attribute.isPrimaryKey) {
				sbr.append(String.format("\t, UNIQUE KEY `%1$s` (`%1$s`)\n", attribute.name));
			}
		}
		for (int i = 0; i < attributes.size(); i++) {
			Attribute attribute = attributes.elementAt(i);
			if (attribute.foreignKey != null && !attribute.isPrimaryKey && !attribute.isUnique) {
				sbr.append(String.format("\t, KEY `%1$s` (`%1$s`)\n", attribute.name));
			}
		}
		sbr.append(String.format(") ENGINE = '%s' DEFAULT CHARSET = '%s' COLLATE = '%s' COMMENT = '%s' AUTO_INCREMENT = %s;\n",
				ENGINE, DEFAULT_CHARSET, COLLATE, comment, AUTO_INCREMENT));
		return sbr.toString();
	}
}
