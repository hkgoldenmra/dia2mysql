
class Attribute {

	Table table = null;
	String name = null;
	String type = null;
	String comment = null;
	boolean isPrimaryKey = false;
	boolean isNullable = false;
	boolean isUnique = false;
	Attribute foreignKey = null;

	Attribute(Table table) {
		this.table = table;
	}

	public String toString() {
		return String.format("`%s` %s %sNULL COMMENT '%s'\n", name, type, isNullable ? "" : "NOT ", comment);
	}
}
